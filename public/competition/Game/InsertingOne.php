<?php
require ('../../../config.php');
require ('../../../common.php');

if (isset($_POST['submit'])) {
    // anders dan Tania, wij escapen in het begin zodat
    // de waarde al 'veilig' is vooraleer naar de database te sturen
  	
    $newUser = array(
    	"Date" => escape($_POST['Date']),
    	"Status"  => escape($_POST['Status']),
    	"ScoreHome"     => escape($_POST['ScoreHome']),
    	"ScoreVisitors"  => escape($_POST['ScoreVisitors']),
    	
    );

    // volgende regel gebuik je op te debuggen (manuele breakpoint)
    // print_r($new_user);
    // niet veilige manier, gevoelig voor SQL injectie
    // $insertSql = "insert into users (firstname, lastname, email) values ('{$new_user['firstname']}', '{$new_user['lastname']}', '{$new_user['email']}')";
    // echo $insertSql;

    // nu met parameters, is veilig
    $insertSql = "insert into Team (Date, Status, ScoreHome, ScoreVisitors) values (:Date, :Status, :ScoreHome, :ScoreVisitors)";
    // echo $insertSql;

    try {
        $connection = new \PDO($host, $user, $password, $options);
        $statement = $connection->prepare($insertSql);
        $statement->execute($newUser);
    } catch (\PDOException $e) {
        echo "Er is iets fout gelopen: {$e->getMessage()}";
    }
}

include ('../../template/header.php'); 
?>
<div id="feedback">
<?php if (isset($_POST['submit']) && $statement) {
    echo $newUser['FirstName'] . ' ' . $newUser['LastName'] . ' is toegevoegd';
}
?>

</div>
<!-- form>(label+input:text)*6 -->
<form action="" method="post">
    <div>
        <label for="Date">Voornaam</label>
        <input type="text" name="Date" id="Date">
    </div>
    <div>
        <label for="Status">Familienaam</label>
        <input type="text" name="Status" id="Status">
    </div>
    <div>
        <label for="ScoreHome">E-mail</label>
        <input type="text" name="ScoreHome" id="ScoreHome">
    </div>
    <div>
        <label for="ScoreVisitors">E-mail</label>
        <input type="text" name="ScoreVisitors" id="ScoreVisitors">
    </div>
    
 
    <input type="submit" value="Verzenden" name="submit">
</form>
    
<?php include ('../../template/footer.php'); ?>