<?php
require ('../../../config.php');
require ('../../../common.php');

$sqlErrorMessage = 'Geen';
if (isset($_POST['submit'])) {
    // anders dan Tania, wij escapen in het begin zodat
    // de waarde al 'veilig' is vooraleer naar de database te sturen

    $newUser = array(
    	"Name" => escape($_POST['Name']),
    	"Location"  => escape($_POST['Location']),
    	"Score"     => escape($_POST['Score'])
    	
    );

    // volgende regel gebuik je op te debuggen (manuele breakpoint)
    // print_r($new_user);
    // niet veilige manier, gevoelig voor SQL injectie
    // $insertSql = "insert into users (firstname, lastname, email) values ('{$new_user['firstname']}', '{$new_user['lastname']}', '{$new_user['email']}')";
    // echo $insertSql;

    // nu met parameters, is veilig
    $insertSql = "insert into Team (Name, Location, Score) values (:Name, :Location, :Score)";
    // echo $insertSql;

    try {
        $connection = new \PDO($host, $user, $password, $options);
        $statement = $connection->prepare($insertSql);
        $statement->execute($newUser);
    } catch (\PDOException $e) {
        $sqlErrorMessage = "Er is iets fout gelopen: {$e->getMessage()}";
    }
}

include ('../../template/header.php'); 
?>

<main>
    <article>
        <header class="command-bar">
            <h2>Liga</h2>
            <nav>
                <button type="submit" value="insert" form="form" name="submit" class="icon-floppy-disk"><span class="screen-reader-text">Insert</span></button></a>
                <a class="icon-cross" href="Index.php"><span class="screen-reader-text">Cancel</span></a>

            </nav>
        </header>
             <!-- form>(label+input:text)*6 -->
            <form action="" method="post">
               <fieldset>
                   <div>
                        <label for="Name">Naam</label>
                        <input type="text" name="Name" id="Name">
                    </div>
                    <div>
                        <label for="Location">Locatie</label>
                        <input type="text" name="Location" id="Location">
                    </div>
                    <div>
                        <label for="Score">Score</label>
                        <input type="text" name="Score" id="Score">
                    </div>
                    <input type="submit" value="Verzenden" name="submit">
                </fieldset>
            </form>
            <div id="feedback">
                <?php 
                   if (isset($_POST['submit']) && $statement) {
                        echo $newUser['Name'] . ' is toegevoegd.<br/>';
                    }
                    echo $sqlErrorMessage;
                ?>
            </div>
    </article>
    <aside>
        <?php include('ReadingAll.php');?>
    </aside>
    </main>
<?php include ('../../template/footer.php'); ?>