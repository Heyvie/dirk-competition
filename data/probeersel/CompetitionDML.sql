USE CompetitionDB;

insert into Game(
    Datum,
    Tijd,
    Status,
    Thuisscore,
    Bezoekersscore
)
values (
   '14-11-2018',
   '20:00',
   'gepland',
   '0',
   '0'
);

insert into Liga(
    Naam,
    Jaar,
    IsInPlanning
)
values (
   'Jupiler League',
   '2018',
   '1'
);

insert into Player(
    Familienaam,
    Voornaam,
    Adres,
    Shirtnummer
)
values (
   'Heyvaert',
   'Dirk',
   'Beekstraat 23 1840 Londerzeel',
   '13'
);

