<?php
namespace KingCluck\Competition\Model;

class Liga
{
    private $name;
    private $year;
    private $isInPlanning;
    
    public function setName($value)
    {
        if (empty($value)) {
            return false;
        } else {
            $this->name = $value;
            return true;
        }
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public function setYear($value)
    {
        if (empty($value)) {
            return false;
        } else {
            $this->year = $value;
            return true;
        }
    }
    
    public function getYear()
    {
        return $this->year;
    }
    
    public function setIsInPlanning($value)
    {
        if (empty($value)) {
            return false;
        } else {
            $this->isInPlanning = $value;
            return true;
        }
    }
    
    public function getIsInPlanning()
    {
        return $this->isInPlanning;
    }
    
}