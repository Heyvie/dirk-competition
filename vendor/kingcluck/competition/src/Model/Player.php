<?php
namespace KingCluck\Competition\Model;

class Player
{
    private $lastName;
    private $firstName;
    private $address;
    private $shirtNumber;
    
    public function setLastName($value)
    {
        if (empty($value)) {
            return false;
        } else {
            $this->lastName = $value;
            return true;
        }
    }
    
    public function getLastName()
    {
        return $this->lastName;
    }
    
    public function setFirstName($value)
    {
        if (empty($value)) {
            return false;
        } else {
            $this->firstName = $value;
            return true;
        }
    }
    
    public function getFirstName()
    {
        return $this->firstName;
    }
    
    public function setAddress($value)
    {
        if (empty($value)) {
            return false;
        } else {
            $this->address = $value;
            return true;
        }
    }
    
    public function getAddress()
    {
        return $this->address;
    }
    
    public function setShirtNumber($value)
    {
        if (empty($value)) {
            return false;
        } else {
            $this->shirtNumber = $value;
            return true;
        }
    }
    
    public function getShirtNumber()
    {
        return $this->shirtNumber;
    }
}