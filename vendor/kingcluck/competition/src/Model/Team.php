<?php
namespace KingCluck\Competition\Model;

class Team
{
    private $name;
    private $location;
    private $score;
    
    public function setName($value)
    {
        if (empty($name)) {
            return false;
        } else {
            $this->name = $value;
            return true;
        }
    }
    
    public function getName()
    {
        return $this->name;
    }
 
    public function setLocation($location)
    {
        if (empty($location)) {
            return false;
        } else {
            $this->location = $location;
            return true;
        }
    }
    
    public function getLocation()
    {
        return $this->location;
    } 
    
    public function setScore($score)
    {
        if (empty($score)) {
            return false;
        } else {
            $this->score = $score;
            return true;
        }
    }
    
    public function getScore()
    {
        return $this->score;
    }
}