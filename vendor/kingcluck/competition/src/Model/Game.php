<?php
namespace KingCluck\Competition\Model;

class Game
{
    private $date;
    private $time;
    private $status;
    private $homeScore;
    private $visitorScore;
    
    //je gebruikt een setter als je wilt controleren
    //of waarde die je wilt toekennen aan een veld
    //een geschikte waarde is
    //indien dat niet nodig is, maak je het veld public
    
    public function setDate($value) 
    {
        if (empty ($value))
        {
            return false;
        } else {
            $this->date = $value;
            return true;
        }
    }
    
    public function getDate() 
    {
        return $this->date;
    }
    
    public function setTime($value) 
    {
        if (empty ($value))
        {
            return false;
        } else {
            $this->time = $value;
            return true;
        }
    }
    
    public function getTime() 
    {
        return $this->time;
    }
    
    public function setStatus($value) 
    {
        if (empty ($value))
        {
            return false;
        } else {
            $this->state = $value;
            return true;
        }
    }
    
    public function getStatus() 
    {
        return $this->state;
    }
    
    public function setHomeScore($value) 
    {
        if (empty ($value))
        {
            return false;
        } else {
            $this->homeScore = $value;
            return true;
        }
    }
    
    public function getHomeScore() 
    {
        return $this->homeScore;
    }
    
    public function setVisitorScore($value) 
    {
        if (empty ($value))
        {
            return false;
        } else {
            $this->visitorScore = $value;
            return true;
        }
    }
    
    public function getVisitorScore() 
    {
        return $this->visitorScore;
    }
}   