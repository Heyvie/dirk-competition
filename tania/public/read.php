<?php 
include ('../config.php');
include ('../common.php');

// volgende mag alleen worden uitgevoerd als er op
// de submit knop is geklikt
if (isset($_POST['submit'])) {
    $location = escape($_POST['location']);
    try {
        $connection = new \PDO($host, $user, $password, $options);
        
        $sqlSelect = "SELECT * from users WHERE location = :location";
        $statement = $connection->prepare($sqlSelect);
        $statement->bindParam(':location', $location, PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetchAll();
        echo 'aantal rijen ' . $statement->rowCount();
    } catch (\PDOException $e) {
        echo "Er is iets fout gelopen: {$e->getMessage()}";
    }                           
}


include ('template/header.php'); ?>

<h2>Zoeken op plaats</h2>
<!-- form>(label+input:text)*6 -->
<form action="" method="post">
    <div>
        <label for="location">Plaats</label>
        <input type="text" name="location" id="location">
    </div>
    <input type="submit" value="Verzenden" name="submit">
</form>

<?php  
if (isset($_POST['submit'])) {
	if ($result && $statement->rowCount() > 0) { 
?>
		<h2>Results</h2>

		<table>
			<thead>
				<tr>
					<th>#</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Email Address</th>
					<th>Age</th>
					<th>Location</th>
					<th>Date</th>
				</tr>
			</thead>
			<tbody>
	<?php foreach ($result as $row) { 
	?>
			<tr>
				<td><?php echo escape($row["id"]); ?></td>
				<td><?php echo escape($row["firstname"]); ?></td>
				<td><?php echo escape($row["lastname"]); ?></td>
				<td><?php echo escape($row["email"]); ?></td>
				<td><?php echo escape($row["age"]); ?></td>
				<td><?php echo escape($row["location"]); ?></td>
				<td><?php echo escape($row["date"]); ?> </td>
			</tr>
		<?php } ?> 
			</tbody>
	</table>
	<?php } else { ?>
		<blockquote>No results found for <?php echo $location; ?>.</blockquote>
	<?php } 
} ?> 
    
<?php include ('template/footer.php'); ?>
